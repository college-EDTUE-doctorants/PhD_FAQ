# 2nd and + inscritpion

See the description in the [ED website](https://ed-tue.osug.fr/spip.php?article116).

## Pedagogique

You have to meet your CT, print the RST and meet the CSI.

### Comité de thèse (CT)

At least 3 members : thesis director(s), + 2 others (1 HDR and 1 outside the
team).

One of them will be the CT referent.

Before mid-septembre.

### Rapport de suivi de thèse (RST)

Adum: http://www.adum.fr/index.pl?site=edtue

### Comité de suivi individuel (CSI)

Meeting with the referent of your CT, one of the lab director, one of the
doctoral school director.

## Administrative

### Fees

As a PhD *student*, you are a student. So, you have to pay inscription fees:

* normal: 380 € = 345 € (inscription) + 34 € (library) (may change from year to
  year)
* If you are registered in another university of grenoble, contact the
  university.

Since the academical year 2018, there is a new fee: the CVEC (90€). 
If we sum up:

| what        | how much |
| ----------- | :------- |
| Inscription | 346 € |
| library     | 34 €  |
| CVEC        | 90 €  |
| **total**   | 470 € |


# Income tax

As a PhD student, you are an employee, have a salary and are subject to the
revenu tax.

Up to now, but it may change soon, income tax in France is not deducted at
source (meaning, deducted by your employer).  Once per year, you will receive a
mail from the french governement for a returning tax.  If you receive any
French income, you are obliged to fill in a declaration of income form to submit
to the tax authorities during before the 20th of May. 

If you are a grant-holder (boursier), you should contact the grantawarding
authorities to find out if you have to make a declaration.


# Social security and health insurance

Sécurité sociale, Assurance complémentaire, whatzat?

The french social security aims to pay for you your medical dispense (drugs,
hospital, etc.).  You do not have to pay for theses services. However you have
to be covered by the social security first.

Given the LAW n°2018-166 of March 8, 2018, namely law ORE “Orientation et
Réussite des Etudiants”, every student registered in a higher educational
establishment is also covered by the student social security regime and is
freely affiliated to the CPAM. 

* Funded and unfunded PhD students will no longer pay anymore 217 € for the
  social security affiliation.
* However, contractual PhD students will continue to pay their affiliation to
  the social security (CPAM, MGEN, etc.) through their income statement.

Although, other academic mutual insurances, such as SMERRA and LMDE, will still
propose complementary policies. Indeed, the social security do not cover *all*
the health care system.  A "mutual" or "complementary" health insurance may do
so.  Please refer to [the health part of the UGA website for international
student](https://international.univ-grenoble-alpes.fr/en/doctoral-student/health/),
which is, to our knowledge, by far the best explanation of the france health
system (even for french people!). 


# Tools we have access to

| tools                | What's for?                  | Where?                                        |
| -------------------  | --------------               | -----------                                   |
| My Core              | Storing and sharing files    | https://mycore.cnrs.fr/                       |
| FileSender (renater) | Transert large file          | https://filesender.renater.fr/                |
| Gitlab               | Web based git repository     | https://gricad-gitlab.univ-grenoble-alpes.fr/ |
| JupyterHub           | Multi-user Jupyter Notebooks | https://jupyterhub.u-ga.fr/hub/login          |

# Useful links

* ED TUE website: https://ed-tue.osug.fr/ (poorly translated...)
* Doctorant@UGA: https://doctorat.univ-grenoble-alpes.fr/
* International@UGA: https://international.univ-grenoble-alpes.fr/ (lots of info! Even for french people)